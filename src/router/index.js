import Vue, { provide } from 'vue'
import VueRouter from 'vue-router'
import WeatherView from '../views/WeatherView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'weather',
    component: WeatherView
  }
]

const router = new VueRouter({
  routes
})

export default router
