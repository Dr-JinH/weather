import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    provinceID: '',
    cityID: '',
  },
  getters: {
  },
  mutations: {
    changeProvinceID(state, provinceID)
    {
      state.provinceID = provinceID
      console.log(state.provinceID)
    },
    changeCityID(state, cityID)
    {
      state.cityID = cityID
      console.log(state.cityID)
    },
  },
  actions: {
  },
  modules: {
  }
})
