const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,

  devServer:
  {
    proxy:
    {
      '^/api1':
      {
        target: 'http://www.weather.com.cn/',
        ws: true,
        changeOrigin: true,
        pathRewrite:
        {
          '^/api1': ''
        }
      },

      '^/api2':
      {
        target: 'https://devapi.qweather.com/',
        ws: true,
        changeOrigin: true,
        pathRewrite:
        {
          '^/api2': ''
        }
      }

    }
  }
})
